package nl.jsprengers.pbt;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class AdmissionCalculatorSuite extends AdmissionCalculatorBase{

    @Test
    void date_in_past_is_invalid(){
        assertThatThrownBy(() -> calculator.getAdmissionForAge(now.minusDays(10)));
    }

    @Test
    void three_year_olds_are_not_admitted(){
        assertThatThrownBy(() -> calculator.getAdmissionForAge(now.minusYears(4).plusDays(1)));
    }

    @Test
    void four_year_olds_are_admitted(){
        calculator.getAdmissionForAge(now.minusYears(4));
    }

    @ParameterizedTest
    @ValueSource(ints = {4, 90})
    public void any_age_between_four_and_ninety_is_valid(int age) {
        assertThat(getAdmissionForAge(age)).isPositive();
    }

    @Test
    public void age_greater_than_90_throws() {
        assertThatThrownBy(() -> getAdmissionForAge(91));
    }

    @Test
    public void age_less_than_zero_throws() {
        assertThatThrownBy(() -> getAdmissionForAge(-1));
    }

    @ParameterizedTest
    @ValueSource(ints = {4, 15})
    public boolean benefit_is_ten_for_minors(int age) {
        return getAdmissionForAge(age) == 10;
    }

    @ParameterizedTest
    @ValueSource(ints = {16, 90})
    public boolean benefit_is_15_for_patrons_over_16(int age) {
        return getAdmissionForAge(age) == 15;
    }
}
