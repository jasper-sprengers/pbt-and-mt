package nl.jsprengers.pbt;

import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.IntRange;
import net.jqwik.api.constraints.Negative;

import static org.assertj.core.api.Assertions.*;

class AdmissionCalculatorPropertySuite extends AdmissionCalculatorBase{

    @Property
    public void any_age_between_four_and_ninety_is_valid(@ForAll @IntRange(min = 4, max = 90) int age) {
        assertThat(getAdmissionForAge(age)).isPositive();
    }

    @Property
    public void age_greater_than_90_throws(@ForAll @IntRange(min = 91) int age) {
        assertThatThrownBy(() -> getAdmissionForAge(age));
    }

    @Property
    public void age_less_than_zero_throws(@ForAll @Negative int age) {
        assertThatThrownBy(() -> getAdmissionForAge(age));
    }

    @Property
    public boolean benefit_is_10_for_minors(@ForAll @IntRange(min = 4, max = 15) int age) {
        return getAdmissionForAge(age) == 10;
    }

    @Property
    public boolean benefit_is_15_for_patrons_over_16(@ForAll @IntRange(min = 16, max = 90) int age) {
        return getAdmissionForAge(age) == 15;
    }
}
