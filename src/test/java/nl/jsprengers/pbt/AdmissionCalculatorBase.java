package nl.jsprengers.pbt;

import java.time.LocalDate;

abstract class AdmissionCalculatorBase {

    protected final LocalDate now = LocalDate.now();
    protected final AdmissionCalculator calculator = new AdmissionCalculator(now);

    int getAdmissionForAge(int age) {
        return calculator.getAdmissionForAge(now.minusYears(age));
    }

}
