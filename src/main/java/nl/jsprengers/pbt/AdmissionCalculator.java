package nl.jsprengers.pbt;

import java.time.LocalDate;
import java.time.Period;

public class AdmissionCalculator {

    private final LocalDate today;

    public AdmissionCalculator(LocalDate now) {
        this.today = now;
    }

    int getAdmissionForAge(LocalDate dateOfBirth) {
        int age = Period.between(dateOfBirth, today).getYears();
        if (age < 0) {
            throw new IllegalArgumentException("date of birth [%s] is not valid".formatted(dateOfBirth));
        } else if (age < 4 || age > 90) {
            throw new IllegalArgumentException("patrons must be between 4 and 90 years old, but is [%s]".formatted(age));
        /*
           Replace the following line with an extra condition:  else if (age < 16 || age >= 65) and run the test suite again
           Notice that the parameterized tests succeed, but any_age_between_four_and_ninety_is_valid in AdmissionCalculatorPropertySuite now captures the extra edge case.
         */
        } else if (age < 16 || age >= 65) {
            return 10;
        } else {
            return 15;
        }
    }


}
